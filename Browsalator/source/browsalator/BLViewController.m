// 
// Copyright 2013 Brian William Wolter, All rights reserved.
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// 

#import "BLViewController.h"
#import "BLDirectionalPanGestureRecognizer.h"

static const CGFloat kBLHalfPi = M_PI / 2.0;

@interface BLViewController (/* Private */)
@property (readwrite, assign) CGFloat percentageRevealed;
@property (readwrite, assign) CGFloat initialPercentageRevealed;
@end

@implementation BLViewController

@synthesize webView = _webView;
@synthesize controlView = _controlView;
@synthesize locationField = _locationField;
@synthesize statusBarSwitch = _statusBarSwitch;
@synthesize navigationSegmentedControl = _navigationSegmentedControl;
@synthesize percentageRevealed = _percentageRevealed;
@synthesize initialPercentageRevealed = _initialPercentageRevealed;

/**
 * Handle memory pressure
 */
-(void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

/**
 * Clean up
 */
-(void)dealloc {
  [_webView release];
  [_controlView release];
  [_locationField release];
  [_statusBarSwitch release];
  [_navigationSegmentedControl release];
  [super dealloc];
}

/**
 * View did load
 */
-(void)viewDidLoad {
  [super viewDidLoad];
  CGRect bounds = self.view.bounds;
  UISwipeGestureRecognizer *swipeRecognizer;
  
  self.controlView.hidden = TRUE;
  self.controlView.frame = CGRectMake(0, 0, bounds.size.width, self.controlView.frame.size.height);
  [self.view insertSubview:self.controlView atIndex:0];
  
  BLDirectionalPanGestureRecognizer *panRecognizer = [[BLDirectionalPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewPanned:)];
  panRecognizer.supportedDirection = kBLPanDirectionVertical;
  panRecognizer.minimumNumberOfTouches = 2;
  panRecognizer.maximumNumberOfTouches = 3;
  [self.webView.scrollView addGestureRecognizer:panRecognizer];
  // don't release yet (used in dependencies below)
  
  UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
  tapRecognizer.numberOfTapsRequired = 2;
  tapRecognizer.numberOfTouchesRequired = 3;
  [self.webView.scrollView addGestureRecognizer:tapRecognizer];
  [tapRecognizer release];
  
  swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewSwipedRight:)];
  swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
  swipeRecognizer.numberOfTouchesRequired = 3;
  [swipeRecognizer requireGestureRecognizerToFail:panRecognizer];
  [self.webView.scrollView addGestureRecognizer:swipeRecognizer];
  [swipeRecognizer release];
  
  swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewSwipedLeft:)];
  swipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
  swipeRecognizer.numberOfTouchesRequired = 3;
  [swipeRecognizer requireGestureRecognizerToFail:panRecognizer];
  [self.webView.scrollView addGestureRecognizer:swipeRecognizer];
  [swipeRecognizer release];
  
  // clean up the pan recognizer (used in dependencies)
  [panRecognizer release];
  
  [self.webView loadRequest:[NSURLRequest requestWithURL:self.defaultPageURL]];
  
}

#pragma mark - Properties

/**
 * Obtain the default page URL
 */
-(NSURL *)defaultPageURL {
  static NSURL *__shared = nil;
  if(__shared == nil) __shared = [[[NSBundle mainBundle] URLForResource:@"content/default.html" withExtension:nil] retain];
  return __shared;
}

/**
 * Obtain the URL scheme matcher expression
 */
-(NSRegularExpression *)URLSchemeExpression {
  static NSRegularExpression *__shared = nil;
  if(__shared == nil) __shared = [[NSRegularExpression alloc] initWithPattern:@"^[\\w\\d\\-]+:" options:0 error:nil];
  return __shared;
}

/**
 * Obtain the percentage revealed
 */
-(CGFloat)percentageRevealed {
  return _percentageRevealed;
}

/**
 * Set the percentage revealed
 */
-(void)setPercentageRevealed:(CGFloat)reveal {
  
  CGRect bounds = self.view.bounds;
  CGRect wframe = self.webView.frame;
  CGRect cframe = self.controlView.frame;
  
  BOOL hidden = (_percentageRevealed = reveal) <= 0;
  BOOL previouslyHidden = self.controlView.hidden;
  self.controlView.hidden = hidden;
  
  if(hidden != previouslyHidden){
    if(hidden) [self controlViewDidBecomeHidden];
    else [self controlViewDidBecomeVisible];
  }
  
  self.webView.frame = CGRectMake(bounds.origin.x, bounds.origin.y + (cframe.size.height * _percentageRevealed), bounds.size.width, wframe.size.height);
  
}

/**
 * Determine if the control view is at least partially visible
 */
-(BOOL)isControlViewVisible {
  return self.percentageRevealed > 0;
}

/**
 * Determine if the control view is mostly visible
 */
-(BOOL)isControlViewRevealed {
  return self.percentageRevealed > 0.5;
}

/**
 * The control view became partially visible
 */
-(void)controlViewDidBecomeVisible {
  // ...
}

/**
 * The control view became completely hidden
 */
-(void)controlViewDidBecomeHidden {
  [self.locationField resignFirstResponder];
}

#pragma mark - Actions

/**
 * Show the control view
 */
-(void)showControlViewAnimated:(BOOL)animated {
  void (^update)(void) = ^ { self.percentageRevealed = 1; };
  if(animated){
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:update completion:NULL];
  }else{
    update();
  }
}

/**
 * Hide the control view
 */
-(void)hideControlViewAnimated:(BOOL)animated {
  void (^update)(void) = ^ { self.percentageRevealed = 0; };
  if(animated){
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:update completion:NULL];
  }else{
    update();
  }
}

/**
 * Reload
 */
-(IBAction)reload:(id)sender {
  if([self.webView request] != nil){
    [self.webView reload];
    [self hideControlViewAnimated:TRUE];
  }
}

/**
 * Go forward
 */
-(IBAction)forward:(id)sender {
  if([self.webView canGoForward]) [self.webView goForward];
}

/**
 * Go back
 */
-(IBAction)back:(id)sender {
  if([self.webView canGoBack]) [self.webView goBack];
}

/**
 * Navigate
 */
-(IBAction)navigate:(id)sender {
  if([sender isKindOfClass:[UISegmentedControl class]]){
    if(((UISegmentedControl *)sender).selectedSegmentIndex == 0){
      [self back:sender];
    }else{
      [self forward:sender];
    }
  }
}

/**
 * Show or hide the status bar
 */
-(IBAction)showStatusBar:(id)sender {
  if(sender != nil && [sender isKindOfClass:[UISwitch class]]){
    [[UIApplication sharedApplication] setStatusBarHidden:!((UISwitch *)sender).on withAnimation:UIStatusBarAnimationSlide];
    [UIView animateWithDuration:0.35 animations:^{
      self.view.window.frame = [[UIScreen mainScreen] applicationFrame];
      self.view.frame = self.view.window.bounds;
    }];
  }
}

#pragma mark - State

/**
 * Update the navigation control
 */
-(void)updateNavigationControl {
  [self.navigationSegmentedControl setEnabled:[self.webView canGoBack] forSegmentAtIndex:0];
  [self.navigationSegmentedControl setEnabled:[self.webView canGoForward] forSegmentAtIndex:1];
}

#pragma mark - Gestures

/**
 * The view was panned
 */
-(void)viewPanned:(UIPanGestureRecognizer *)gestureRecognizer {
  if(gestureRecognizer.state == UIGestureRecognizerStateBegan){
    self.initialPercentageRevealed = self.percentageRevealed;
  }else if(gestureRecognizer.state == UIGestureRecognizerStateChanged){
    CGFloat ty = [gestureRecognizer translationInView:self.view].y;
    CGFloat dy = self.controlView.frame.size.height;
    CGFloat by = self.initialPercentageRevealed + (ty / dy);
    CGFloat vy = 1, zy = 4;
    CGFloat oy = (by > 1) ? MAX(0, MIN(vy, sinf(MAX(0, MIN(1, (ty - dy) / (dy * zy))) * kBLHalfPi) * vy)) : 0;
    self.percentageRevealed = MAX(0, MAX(-1, MIN(1, by)) + oy);
  }else if(gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled){
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{ self.percentageRevealed = [self isControlViewRevealed] ? 1 : 0; } completion:NULL];
  }
}

/**
 * The view was tapped
 */
-(void)viewTapped:(UITapGestureRecognizer *)gestureRecognizer {
  if(gestureRecognizer.state == UIGestureRecognizerStateRecognized){
    [self reload:nil];
  }
}

/**
 * The view was swiped
 */
-(void)viewSwipedRight:(UITapGestureRecognizer *)gestureRecognizer {
  if(gestureRecognizer.state == UIGestureRecognizerStateRecognized){
    [self back:gestureRecognizer];
  }
}

/**
 * The view was swiped
 */
-(void)viewSwipedLeft:(UITapGestureRecognizer *)gestureRecognizer {
  if(gestureRecognizer.state == UIGestureRecognizerStateRecognized){
    [self forward:gestureRecognizer];
  }
}

#pragma mark - Web view delegate

/**
 * Will start load
 */
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
  if(webView == self.webView){
    self.locationField.text = [request.URL absoluteString];
    [self updateNavigationControl];
  }
  return TRUE;
}

/**
 * Finished load
 */
-(void)webViewDidFinishLoad:(UIWebView *)webView {
  if(webView == self.webView){
    self.locationField.text = [webView.request.URL absoluteString];
    [self updateNavigationControl];
  }
}

/**
 * Load failed
 */
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Could Not Load" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
  [alertView show];
  [alertView release];
}

#pragma mark - Text field delegate

/**
 * Return action
 */
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
  NSString *url;
  if((url = textField.text) != nil && [url length] > 0){
    if([self.URLSchemeExpression rangeOfFirstMatchInString:url options:0 range:NSMakeRange(0, [url length])].location == NSNotFound) url = [NSString stringWithFormat:@"http://%@", url];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [textField resignFirstResponder];
  }
  return TRUE;
}

@end
