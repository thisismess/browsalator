// 
// Copyright 2013 Brian William Wolter, All rights reserved.
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// 

#import <UIKit/UIGestureRecognizerSubclass.h>

#import "BLDirectionalPanGestureRecognizer.h"

@interface BLDirectionalPanGestureRecognizer (/* Private */)
@property (readwrite, assign) CGPoint translation;
@property (readwrite, assign) BOOL    gestureBegan;
@end

@implementation BLDirectionalPanGestureRecognizer

@synthesize supportedDirection = _supportedDirection;
@synthesize translation = _translation;

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
  [super touchesMoved:touches withEvent:event];
  if(self.state == UIGestureRecognizerStatePossible){
    CGPoint location = [[touches anyObject] locationInView:self.view];
    CGPoint previous = [[touches anyObject] previousLocationInView:self.view];
    CGPoint translation = CGPointMake(location.x - previous.x, location.y - previous.y);
    if(fabsf(translation.x) > fabsf(translation.y)){
      if(self.supportedDirection != kBLPanDirectionHorizontal){
        self.state = UIGestureRecognizerStateFailed;
      }
    }else{
      if(self.supportedDirection != kBLPanDirectionVertical){
        self.state = UIGestureRecognizerStateFailed;
      }
    }
  }
}

-(void)reset {
  [super reset];
  _translation = CGPointZero;
}

@end
