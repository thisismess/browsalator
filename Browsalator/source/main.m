//
//  main.m
//  Browsalator
//
//  Created by Brian William Wolter on 8/27/13.
//  Copyright (c) 2013 Brian William Wolter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([BLAppDelegate class]));
  }
}
